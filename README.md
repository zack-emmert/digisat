# Digital Satellites

Digital Satellites is an addon for the Advanced Rocketry and OpenComputers mods for Minecraft 1.12.2. It adds GPS 
Upgrades for robots, drones, and tablets, that use GPS Satellites to provide global positioning.

# GPS Satellites

In order to use a GPS Upgrade, you will need to launch at least three GPS Satellites. The accuracy of the coordinates
the GPS Upgrade will return improves as more satellites are used, providing perfect accuracy at 16 satellites. A 
GPS Satellite is created by placing a GPS Upgrade (See JEI for the crafting recipe) into the top-right slot of the 
Satellite Builder. By default, GPS Satellites consume 10 FE every time a device connects to them (configurable). If a
GPS Satellite does not have enough power, it will not be counted among the connected satellites when determining accuracy.

# OpenComputers API

```lua
local component = require("component")
local gps = component.gps

-- Adds all passed satellite IDs to the list of satellites the upgrade will connect to in order to provide coordinates.
-- Takes int..., returns true if successful, and false, reason in case of error.
gps.addSatellites(0,1,2,3)

-- Removes all passed satellite IDs from the list of satellites the upgrade will connect to in order to provide coordinates.
-- Takes int..., returns true if successful, and false, reason in case of error.
-- Will return true if the removed satellite was not in the list.
gps.removeSatellites(0,1)

-- Removes all satellites from the list of satellites the upgrade will connect to in order to provide coordinates.
-- Takes no parameters, returns nil.
gps.clearSatellites()

-- Retrieves the list of satellites the upgrade will connect to in order to provide coordinates.
-- Takes no parameters, returns an array of integers corresponding to satellite IDs.
local satellites = gps.getSatellites()

-- Gets the current global coordinates of the callee.
-- Takes no parameters, returns a table with keys "x", "y", "z" containing numbers corresponding to each positional axis.
local coords = gps.getWorldPos()
```

# Credits

 - SolrackIcarus
 - Sílfryí Kalsandryn
 - slava_110
 - ben_mkiv
 - Sangar
 - zmaster587
 - Hannuh 
 - The Advanced Rocketry and OpenComputers developers

### Copyright

Advanced Rocketry and OpenComputers are copyrights of their respective owners.
