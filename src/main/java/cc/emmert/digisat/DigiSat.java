package cc.emmert.digisat;

import cc.emmert.digisat.common.CommonProxy;
import cc.emmert.digisat.common.driver.DriverGPSUpgrade;
import cc.emmert.digisat.common.satellite.SatelliteGPS;
import zmaster587.advancedRocketry.api.SatelliteRegistry;
import li.cil.oc.api.Driver;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import org.apache.logging.log4j.Logger;

@Mod.EventBusSubscriber
@Mod(modid = DigiSat.MODID, name = "Digital Satellites", dependencies = "required-after:opencomputers;required-after:advancedrocketry")
public class DigiSat {
	public static final String MODID = "digisat";

	private static Logger logger;

	public static Logger getLogger() { return logger; }

	public static CommonProxy proxy;

	@Mod.EventHandler
	public void preInit(FMLPreInitializationEvent event) {
		logger = event.getModLog();
		SatelliteRegistry.registerSatellite(SatelliteGPS.TYPE, SatelliteGPS.class);
	}

	@Mod.EventHandler
	public void init(FMLInitializationEvent event) {
		Driver.add(new DriverGPSUpgrade());
	}
}
