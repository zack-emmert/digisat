package cc.emmert.digisat.client;

import cc.emmert.digisat.common.CommonProxy;
import cc.emmert.digisat.common.item.ItemGPSUpgrade;
import net.minecraftforge.client.event.ModelRegistryEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.relauncher.Side;

@Mod.EventBusSubscriber(Side.CLIENT)
public class ClientProxy extends CommonProxy {

	@SubscribeEvent
	public static void registerModels(ModelRegistryEvent event) {
		ItemGPSUpgrade.ITEM.initModel();
	}
}
