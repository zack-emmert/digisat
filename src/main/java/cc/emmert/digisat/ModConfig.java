package cc.emmert.digisat;

import net.minecraftforge.common.config.Config;
import net.minecraftforge.common.config.Config.*;

@Config(modid = DigiSat.MODID, name = "digitalsatellites")
public class ModConfig {
	@Comment({
		"Power usage per operation for the GPS Transmitter Satellite."
	})
	@Name("Satellite Power use")
	@RangeInt(min = 0)
	@RequiresMcRestart
	public static int satellitePowerUse = 10;

	@Comment({
		"Power usage per operation for the GPS Upgrade."
	})
	@Name("GPS Upgrade Power use")
	@RangeInt(min = 0)
	@RequiresMcRestart
	public static int gpsPowerUse = 10;

	@Comment({
		 "Parameter used to configure the average coordinate drift formula",
		 "Formula is: constant / n^exponent, where n is the number of connected satellites"
	})
	@Name("Drift Range Constant")
	@RangeDouble(min=0.0)
	@RequiresMcRestart
	public static double driftConstant=3469.0;
	@Name("Drift Range Exponent")
	@RangeDouble(min=0.0)
	@RequiresMcRestart
	public static double driftExponent=3.0;

	@Comment({
		 "Disable coordinate inaccuracies. This will make 3 satellites give perfect accuracy every time"
	})
	@Name("Easy Mode")
	@RequiresMcRestart
	public static boolean easyMode = false;
}
