package cc.emmert.digisat.common.item;

import cc.emmert.digisat.DigiSat;
import li.cil.oc.api.CreativeTab;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class ItemGPSUpgrade extends Item {
	public static final ItemGPSUpgrade ITEM = new ItemGPSUpgrade();
	public ItemGPSUpgrade() {
		super();
		this.setRegistryName(new ResourceLocation(DigiSat.MODID, "gps_upgrade"))
				.setUnlocalizedName(DigiSat.MODID+".gps_upgrade")
				.setCreativeTab(CreativeTab.instance);
	}

	@SideOnly(Side.CLIENT)
	public void initModel() {
		ModelLoader.setCustomModelResourceLocation(this,0,new ModelResourceLocation("digisat:itemgpsupgrade"));
	}
}
