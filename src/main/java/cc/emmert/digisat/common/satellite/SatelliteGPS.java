package cc.emmert.digisat.common.satellite;

import cc.emmert.digisat.ModConfig;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import zmaster587.advancedRocketry.api.satellite.SatelliteBase;
import zmaster587.libVulpes.util.ZUtils;

public class SatelliteGPS extends SatelliteBase {
	public static final String TYPE = "gps";
	public String getInfo(World world) {
		return "Energy Capacity: " + ZUtils.formatNumber(this.satelliteProperties.getPowerStorage()) + "\n" +
				"Energy Consumption: " + ZUtils.formatNumber(ModConfig.satellitePowerUse) + "\n" +
				"Energy Production: " + ZUtils.formatNumber(this.satelliteProperties.getPowerGeneration());
	}

	public String getName() {
		return "GPS Transmitter";
	}

	public boolean performAction(EntityPlayer entityPlayer, World world, BlockPos blockPos) {
		return false;
	}

	public double failureChance() {
		return 0;
	}

	public boolean usePower() {
		int remainingPower = this.satelliteProperties.getPowerStorage();

		if(remainingPower < ModConfig.satellitePowerUse) {
			return false;
		} else {
			this.satelliteProperties.setPowerStorage(remainingPower - ModConfig.satellitePowerUse);
			return true;
		}
	}
}
