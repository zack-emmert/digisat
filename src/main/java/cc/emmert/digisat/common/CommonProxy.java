package cc.emmert.digisat.common;

import cc.emmert.digisat.DigiSat;
import cc.emmert.digisat.common.item.ItemGPSUpgrade;
import cc.emmert.digisat.common.satellite.SatelliteGPS;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import zmaster587.advancedRocketry.api.SatelliteRegistry;
import zmaster587.advancedRocketry.api.satellite.SatelliteProperties;

@Mod.EventBusSubscriber(modid = DigiSat.MODID)
public class CommonProxy {
	@SubscribeEvent
	public static void registerItems(RegistryEvent.Register<Item> event) {
		System.out.println("Registering DigiSat Items!");
		event.getRegistry().registerAll(
				ItemGPSUpgrade.ITEM
		);
		SatelliteRegistry.registerSatelliteProperty(new ItemStack(ItemGPSUpgrade.ITEM,1,0),new SatelliteProperties().setSatelliteType(SatelliteGPS.TYPE));
	}
}
