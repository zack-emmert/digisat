package cc.emmert.digisat.common.driver;

import cc.emmert.digisat.ModConfig;
import cc.emmert.digisat.common.item.ItemGPSUpgrade;
import cc.emmert.digisat.common.satellite.SatelliteGPS;
import li.cil.oc.api.Network;
import li.cil.oc.api.driver.item.Slot;
import li.cil.oc.api.internal.Drone;
import li.cil.oc.api.internal.Robot;
import li.cil.oc.api.internal.Tablet;
import li.cil.oc.api.machine.Arguments;
import li.cil.oc.api.machine.Callback;
import li.cil.oc.api.machine.Context;
import li.cil.oc.api.network.Connector;
import li.cil.oc.api.network.Visibility;
import li.cil.oc.api.prefab.DriverItem;
import li.cil.oc.api.driver.item.HostAware;
import li.cil.oc.api.network.EnvironmentHost;
import li.cil.oc.api.network.ManagedEnvironment;
import li.cil.oc.api.prefab.AbstractManagedEnvironment;
import net.minecraft.item.ItemStack;
import zmaster587.advancedRocketry.api.AdvancedRocketryAPI;
import zmaster587.advancedRocketry.api.satellite.SatelliteBase;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Provides the OpenComputers driver & Environment for the GPS Upgrade
 */
public class DriverGPSUpgrade extends DriverItem implements HostAware {

	public DriverGPSUpgrade() {
		super(new ItemStack(ItemGPSUpgrade.ITEM));
	}

	public ManagedEnvironment createEnvironment(ItemStack itemStack, EnvironmentHost environmentHost) {
		return new Environment(environmentHost);
	}

	public String slot(ItemStack itemStack) {
		return Slot.Upgrade;
	}

	public int tier(ItemStack itemStack) {
		return 1;
	}

	public boolean worksWith(ItemStack itemStack, Class<? extends EnvironmentHost> host) {
		if(host.isAssignableFrom(Drone.class) || host.isAssignableFrom(Robot.class) || host.isAssignableFrom(Tablet.class)) {
			return this.worksWith(itemStack);
		} else {
			return false;
		}
	}

	/**
	 * The Environment that provides OpenComputers callbacks for the GPS Upgrade
	 */
	public static class Environment extends AbstractManagedEnvironment {
		Random rand;

		// The upgrade's current host
		EnvironmentHost host;

		// All the satellites the upgrade will connect to
		Set<Long> satelliteIDs;

		// A Connector is just a node that can push/pull power from the network
		Connector node;

		public Environment(EnvironmentHost host) {
			this.host = host;
			this.rand = new Random();
			this.satelliteIDs = new HashSet<>();
			this.node = Network.newNode(this, Visibility.Neighbors).withComponent("gps").withConnector().create();
			// This is needed by the upstream AbstractManagedEnvironment
			this.setNode(this.node);
		}

		@Callback(doc = "function():table Get the host's current position in the world. Returns a table with keys x, y, z corresponding to in-world coordinates. " +
								"Accuracy increases with more satellites in orbit. Will return false, reason in case of error")
		public Object[] getWorldPos(final Context _context, Arguments _arguments) {

			if(!node.tryChangeBuffer(-ModConfig.gpsPowerUse)) {
				return new Object[] {false,"Error: Not enough power"};
			}

			HashMap<String,Double> coordinates = new HashMap<>();

			// Retrieves the SatelliteBase instance for each ID, counting the number of successful connections.
			//	This happens once per call instead of at registration in case the satellite state changes
			//	between registration and use. See `DriverGPSUpgrade.Environment.canConnectTo` for conditions for
			//	a successful connection
			long satelliteCount = this.satelliteIDs.stream()
										  .map(id->AdvancedRocketryAPI.dimensionManager.getSatellite(id))
										  .filter(this::canConnectTo)
										  .count();

			if (satelliteCount<3) {
				return new Object[]{false,"Unable to connect to enough satellites"};
			} else if (satelliteCount<16 && !ModConfig.easyMode) {
				// Computes the maximum possible drift according to the formula explained in the config. The max is double the average,
				//	hence, the * 2.0
				double driftRange = ModConfig.driftConstant * 2.0/Math.pow((double) satelliteCount,ModConfig.driftExponent);

				// Computes three values between +- driftRange, and adds them to the coordinates
				List<Double> driftValues = rand.doubles(3,-driftRange,driftRange).boxed().collect(Collectors.toList());

				coordinates.put("x", this.host.xPosition() + driftValues.get(0));
				coordinates.put("y", this.host.yPosition() + driftValues.get(1));
				coordinates.put("z", this.host.zPosition() + driftValues.get(2));
			} else {
				coordinates.put("x", this.host.xPosition());
				coordinates.put("y", this.host.yPosition());
				coordinates.put("z", this.host.zPosition());
			}

			return new Object[]{coordinates};
		}

		@Callback(doc = "function(int...):void Adds GPS satellites to the component's registry. Takes satellite IDs. Returns true if registration was successful and false, reason otherwise")
		public Object[] addSatellites(final Context _context, Arguments arguments) {
			if(arguments.count()<1) {
				return new Object[]{false,"Wrong number of arguments"};
			}

			for(int i = 0; i < arguments.count(); i++) {
				int id = arguments.checkInteger(i);
				this.satelliteIDs.add((long) id);
			}
			return new Object[]{true};
		}

		@Callback(doc = "function(int...):void Removes GPS satellites from the component's registry. Takes satellite IDs. Returns true if removal was successful and false, reason otherwise")
		public Object[] removeSatellites(final Context _context, Arguments arguments) {
			if(arguments.count()<1) {
				return new Object[]{false,"Wrong number of arguments"};
			}

			for(int i = 0; i < arguments.count(); i++) {
				int id = arguments.checkInteger(i);
				this.satelliteIDs.remove((long) id);
			}
			return new Object[]{true};
		}

		@Callback(doc = "function():void Clears the component's satellite registry.")
		public Object[] removeAllSatellites(final Context _context, Arguments _arguments) {
			this.satelliteIDs.clear();
			return new Object[]{};
		}

		@Callback(doc = "function():table Retrieves the set of currently registered satellite IDs. Order is undefined.")
		public Object[] getSatellites(final Context _context, Arguments arguments) {
			return new Object[]{this.satelliteIDs.toArray()};
		}

		private boolean canConnectTo(SatelliteBase sat) {
			return sat instanceof SatelliteGPS
						   && !sat.isDead()
						   && sat.getDimensionId() == this.host.world().provider.getDimension()
						   && ((SatelliteGPS) sat).usePower()
			;
		}
	}
}
